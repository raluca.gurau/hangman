#include <iostream>
#include<string>
using namespace std;

int main()
	{
	int num_guesses = 6;
	string dashes = "-------";
	string word = "hangman";
	cout << "Enter an 6 character word for your oponent: ";
	cin >> word;
	string guess;
	while (num_guesses > 0 && dashes != word) 
	{
		cout << dashes << endl;
		cout << num_guesses << " incorrect guesses left. " << endl;
		cout << "Guess: ";
		getline(cin, guess);
		if (guess.length() > 1) 
		{
			cout << "That is more than one character.";
		}
		else if (guess[0] > 'z' || guess[0] < 'a') {
			cout << "Your guess must be a lowercase letter. ";
		}
		else {
			size_t index_found = word.find(guess);
			if (index_found != string::npos) 
			{
				for (int i = 0; i < word.length(); i++) 
				{
					if (word[i] == guess[0]) 
					{
						dashes[i] = guess[0];
					}
				}
				cout << "Correct!" << endl;
			}
			else {
				cout << "Incorrect!" << endl;
				num_guesses = num_guesses - 1;
			}
		}
	}
	if (dashes == word) 
	{
		cout << "You win!" << endl;
		cout << "The word was: " << word << endl;
	}
	else 
	{
		cout << "You lose." << endl;
		cout << "The word was: " << word << endl;
	}
}